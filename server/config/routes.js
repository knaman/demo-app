module.exports.routes = {
  '/': {
      view: 'landing'
  },

  '/signup/:token': {
      view: 'signup-auth'
  },

  '/reset/:token': {
      view: 'reset-auth'
  },

  '/profile': {
      view: 'profile'
  },

  '/map': {
      view: 'map'
  },

  'post /api/accounts/login': 'AuthController.processLogin',
  'post /api/accounts/login/facebook': 'AuthController.processLoginFacebook',
  'post /api/accounts/reset': 'AuthController.resetPassword',
  'post /api/accounts/reset-done': 'AuthController.resetPasswordDone',

  'get /api/accounts/logout': 'AuthController.logout',
  'get /api/accounts/signup/:token': 'AuthController.verifySignupToken',
  'get /api/accounts/reset/:token': 'AuthController.verifyResetToken',
  'get /api/accounts/is_logged_in': 'AuthController.isLoggedIn',

  'get /connect/linkedin': 'AuthController.connectLinkedin',
  'get /linkedin/auth': 'AuthController.connectLinkedinAuth',

  'get /api/teacher': 'MapController.findTeachers',
  'get /api/student': 'MapController.findStudents',

};
