var Promise = require("bluebird");

module.exports = {
  findTeachers: function(req, res, next) {
    var GetNear =  sails.services.getnear;

    if (typeof req.user.latitude === 'undefined') {
      return res.badRequest({error: 'Please set your address'});
    }

    User.find({userType: 'teacher'})
      .then(function(teachers) {
        res.send({
          others: GetNear.run(req.user, teachers),
          user: req.user
        });
      })
      .catch(function(err) {
        next(err);
      });
  },

  findStudents: function(req, res,next) {
    var GetNear =  sails.services.getnear;

    if (typeof req.user.latitude === 'undefined') {
      return res.badRequest({error: 'Please set your address'});
    }

    User.find({userType: 'student'})
      .then(function(students) {
        console.log(students);
        res.send({
          others: GetNear.run(req.user, students),
          user: req.user
        });
      })
      .catch(function(err) {
        next(err);
      });
  }
};