 /* AuthController
 *
 * @description :: Server-side logic for managing Auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var Promise = require("bluebird");
var crypto = Promise.promisifyAll(require("crypto"));
var bcrypt = Promise.promisifyAll(require('bcrypt'));

module.exports = {

  verifySignupToken: function(req, res) {
    var token = req.params.token;

    User.findOne({token: token})
      .then(function(user) {
        if (user) {
          user.isAuth = true;

          user.save()
            .then(function() {
              res.ok({message: user.email + ' authenticated'});
            })
            .catch(function(err){
              next(err);
            });

        } else {
          res.unauth({error: 'Invalid token'});
        }
      })
      .catch(function(err) {
        return next(err);
      });
  },

  processLogin: function(req, res, next) {
    var passport = sails.services.passport;

    passport.authenticate('local', function(err, user, info) {
      if (err)  {
        return next(err);
      }

      if (user) {
        req.logIn(user, function(err) {
          if (err) {
            return next(err);
          }

          res.ok({message: 'Login success.', user: req.user});
        });
      } else {
        res.status(info.status).json({error: info.error});
      }
    })(req, res);
  },

  processLoginFacebook: function(req, res, next) {
    var passport = sails.services.passport;
    passport.authenticate('facebook-token', function(err, user) {
      if (err)
      return next(err);

      req.logIn(user, function(err) {
        if (err) {
          return next(err);
        }

        res.ok({message: 'Login success.', user: req.user});
      });
    })(req, res);
  },

  resetPassword: function(req, res) {
    var EmailService = sails.services.email;
    var emailBody = 'Please click on the following link, or paste this into your' +
      'browser to reset passowrd:\n\n' + sails.config.BASEURL + '/reset/';

    User.findOne({email: req.body.email})
      .then(function(user){
        if (user) {
          if (!user.isAuth) {
            return res.unauth({error: 'Please authenticate.'});
          }

          var message = 'Great! An email has been sent to ' + user.email + '.';

          res.ok({message: message});

          crypto.randomBytesAsync(12)
            .then(function(buf) {
              user.token = buf.toString('hex');
              emailBody += user.token;
              EmailService.send(user.email, emailBody, 'Reset password', function(err) {
                if (err) return sails.log.error(err);
              });

              user.save()
                .catch(function() {
                  return next(err);
                });
            })
            .catch(function(err) {
              return next(err);
            });

        } else {
          res.notFound({error: 'User not found.'})
        }
      })
      .catch(function(err) {
        return next(err);
      });
  },

  resetPasswordDone: function(req, res) {
    var pswd = req.body.password;
    var pswdAgain = req.body.password_again;
    var token = req.body.token;

    if (pswd !==  pswdAgain)
    return res.badRequest({error: 'Passwords not same.'});

    User.findOne({token: token})
      .then(function(user) {
        if (user) {
          bcrypt.genSaltAsync(10)
            .then(function(salt) {
              return bcrypt.hashAsync(pswd, salt);
            })
            .then(function(result) {
              user.password = result;
              return user.save();
            })
            .then(function(user) {
              return res.ok({message: 'Password reset success.', user: user});
            })
            .catch(function(err) {
              return next(err);
            })
        } else {
          res.unauth({error: 'Invalid token'});
        }
      })
      .catch(function(err) {
        return next(err);
      });
  },

  verifyResetToken: function(req, res) {
    var token = req.params.token;

    User.findOne({token: token})
      .then(function(user) {
        if (user) {
          res.ok({message: 'token valid', user: user});
        } else {
          res.unauth({error: 'Invalid token'});
        }
      })
      .catch(function(err) {
        return next(err);
      });
  },

  isLoggedIn: function(req, res) {
    if (req.user) {
      res.ok({message: 'Logged in.',  user: req.user});
    } else {
      res.unauth({error: 'User is not logged in.'});
    }
  },

  logout: function(req, res) {
    req.logout();

    res.ok({message: 'logout success'});
  },

  connectLinkedin: function(req, res) {
    var passport = sails.services.passport;

    passport.authenticate('linkedin')(req, res);
  },

  connectLinkedinAuth: function(req, res, next) {
    var passport = sails.services.passport;

    passport.authenticate('linkedin', function(err, user) {
      if (err)
      return next(err);

      res.redirect('/profile');
    })(req, res);
  },
};
