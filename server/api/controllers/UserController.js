/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  _config: {
    restPrefix: '/api/accounts'
  },

	create: function(req, res, next) {
    var EmailService = sails.services.email;
    var emailBody = 'Please click on the following link, or paste this into your' +
      'browser to authenticate:\n\n' +
      sails.config.BASEURL + '/signup/';

    User.create(req.body)
      .then(function(user) {
        var message = 'Great! An email has been sent to ' + user.email + '.';

        res.ok({message: message, user: user});
        return user;
      })
      .then(function(user) {
        emailBody += user.token;
        EmailService.send(user.email, emailBody, 'Email Authentication', function(err) {
          if (err) return sails.log.error(err);
        });
      })
      .catch(function(err){
        res.badRequest({error: err.Errors});
      });
  },

  update: function(req, res, next) {
    var userUpdate = _.omit(req.body, 'skills', 'headline', 'industry');

    User.update({id: req.user.id}, userUpdate)
      .then(function(user) {
        var _user = user[0];

        if (req.body.skills) {
          Linkedin.update({id: _user.linkedInDetails},{
              skills: JSON.parse(req.body.skills),
              headline: req.body.headline,
              industry: req.body.industry,
            })
            .then(function(linkedin) {
              return res.ok({message: 'Update success', user: _user});
            })
            .catch(function(err) {
              return next(err);
            });
        }
        else
        return res.ok({message: 'Update success', user: _user});
      })
      .catch(function(err) {
        next(err);
      });
  },
};

