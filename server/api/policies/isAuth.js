module.exports = function isAuth (req, res, next) {
  if (req.user)
  return next();
  else
  res.unauth({error: 'Please login to proceed.'});
}