var geolib = require('geolib');
var _  = require('lodash');

module.exports = {
  run: function(user, others) {
    var radius = 1000;
    var near = [];
    var dist;

    _.each(others, function(other) {
      if (typeof other.latitude !== 'undefined') {
        dist = geolib.getDistance(
          {latitude: other.latitude, longitude: other.longitude},
          {latitude: user.latitude, longitude: user.longitude}
        );

        if (dist - radius < 0)
        near.push(other);
      }
    });

    return near;
  },
}