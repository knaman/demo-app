var nodemailer = require('nodemailer');

module.exports = {
  send: function(toAddr, body, subject, cb) {
    if (sails.config.IS_LOCAL)
    return cb();

    var smtpTransport = nodemailer.createTransport('SMTP', {
      service: 'Mandrill',
      auth: {
        user: sails.config.MANDRILLUSER,
        pass: sails.config.MANDRILLPASS,
      }
    });

    var mailOptions = {
      to: toAddr,
      from: 'sample-app@demo.com',
      subject: subject,
      text: body,
    };

    smtpTransport.sendMail(mailOptions, function(err) {
      if (err) return cb(err);

      cb();
      console.log('email sent');
    });
  },
}