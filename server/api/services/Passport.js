var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var FacebookTokenStrategy = require('passport-facebook-token');
var LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findOne({ id: id } , function (err, user) {
    done(err, user);
  });
});

passport.use('local', new LocalStrategy({
    usernameField : 'email',
    passwordField : 'password',
},
function(email, password, done) {

  User.findOne({ email: email })
    .then(function(user) {
      if (user) {
        if (!user.isAuth) {
          return done(null, false, {status: 401, error: 'Please authenticate.'});
        }

        if (!user.password) {
         return done(null, false, {status: 401, error: 'Please reset password.'});
        }

        user.comparePswd(password)
          .then(function(result) {
            if (result) {
              return done(null, user);
            } else {
              return done(null, false, {status: 401, error: 'Invalid credentials.'});
            }
          })
          .catch(function(err) {
            return done(err);
          });
      } else {
        return done(null, false, {status: 404, error: 'User not found'});
      }
    })
    .catch(function(err) {
      return done(err);
    });
}));

passport.use('facebook-token', new FacebookTokenStrategy({
    clientID: sails.config.FACEBOOK_APP_ID,
    clientSecret: sails.config.FACEBOOK_APP_SECRET,
  }, function(accessToken, refreshToken, profile, done) {

    var profileData = profile._json;

    User.findOne({email: profileData.email}, function (err, user) {
      if (err)
      return done(err);

      // first time
      // first create a facebook user and
      // then create  a normal user and attach the facebook user
      if (!user) {
        Facebook.create({
          email: profileData.email,
          accessToken: accessToken,
          refreshToken: refreshToken
        })
          .then(function(fb){
            return User.create({email: profileData.email, facebookDetails: fb.id, isAuth: true});
          })
          .then(function(user){
            done(null, user);
          })
          .catch(function(err){
            done(err);
          });
      } else {

        // email aready exist
        // if already signed using facebook
        if (user.facebookDetails) {
          return done(null, user);
        }

        // if already signed using legacy
        Facebook.create({
          email: profileData.email,
          accessToken: accessToken,
          refreshToken: refreshToken
        })
          .then(function(fb){
            user.facebookDetails = fb.id;
            return user.save();
          })
          .then(function(user) {
            done(null, user);
          })
          .catch(function(err) {
            done(err);
          });
      }
    });
  }
));

passport.use('linkedin', new LinkedInStrategy({
  clientID: sails.config.LINKEDIN_KEY,
  clientSecret: sails.config.LINKEDIN_SECRET,
  state: true,
  callbackURL: sails.config.BASEURL + '/linkedin/auth/',
  scope: ['r_emailaddress', 'r_basicprofile'],
  passReqToCallback : true,
}, function(req, accessToken, refreshToken, profile, done) {
    // asynchronous verification, for effect...
  process.nextTick(function () {
    var json = profile._json;

    User.findOne({id: req.user.id})
      .then(function(user) {
        Linkedin.create({
          email: json.emailAddress,
          industry: json.industry,
          headline: json.headline,
        })
        .then(function(linkedin) {
          user.linkedInDetails = linkedin.id;

          return user.save();
        })
        .then(function(user) {
          done(null, user)
        })
        .catch(function(err){
          return done(err);
        });
      })
      .catch(function(err) {
        return done(err);
      });
  });
}));

module.exports = passport;
