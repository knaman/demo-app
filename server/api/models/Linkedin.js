var _  = require('lodash');

var skills = [
  {name: 'HTML'},
  {name: 'CSS'},
  {name: 'Javascript'},
  {name: 'Python'},
  {name: 'Project Management'},
];

module.exports = {
  attributes: {
    email: 'email',
    headline: 'string',
    industry: 'string',
    skills: 'array',
  },

  beforeCreate: function(values, cb) {
    values.skills = skills;
    cb();
  },
};