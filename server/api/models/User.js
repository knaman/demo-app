var Promise = require("bluebird");
var crypto = Promise.promisifyAll(require("crypto"));
var bcrypt = Promise.promisifyAll(require('bcrypt'));

module.exports = {
  attributes: {
    email: {
      type: 'email',
      required: true,
      unique: true
    },
    userType: {
      type: 'string',
      defaultsTo: 'student',
    },
    editMode: {
      type: 'boolean',
      defaultsTo: true,
    },

    firstName: 'string',
    lastName: 'string',
    address: 'string',
    latitude: 'string',
    longitude: 'string',

    password: {
      type: 'string',
    },
    isAuth: {
      type: 'boolean',
      defaultsTo: false,
    },
    token: 'string',
    facebookDetails: {
      model: 'facebook'
    },
    linkedInDetails: {
      model: 'linkedin'
    },

    comparePswd: function(pswd) {
      return bcrypt.compareAsync(pswd, this.password)
    },

    toJSON: function() {
      var obj = this.toObject();

      delete obj.password;
      delete obj.token;
      delete obj.isAuth;

      return obj;
    },
  },

  beforeCreate: function(values, next) {
    if (!values.password) {
      next();
      return;
    }

    bcrypt.genSaltAsync(10)
      .then(function(salt) {
         return bcrypt.hashAsync(values.password, salt);
      })
      .then(function(result) {
        values.password = result;
        return true;
      })
      .catch(function(err) {
        return next(err);
      })
      .then(function() {
        return crypto.randomBytesAsync(12);
      })
      .then(function(buf) {
        values.token = buf.toString('hex');
        next();
      })
      .catch(function(err) {
        next(err);
      });
  },

  validationMessages: {
    email: {
      required: 'Email is required',
      email: 'Provide valid email address',
      unique: 'Email address is already taken'
    }
  }
};