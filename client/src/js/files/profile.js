var $ = require('jquery');
var Backbone = require('backbone');
var _ = require('underscore');
var FormValidator = require('form-validatorjs');
var cookie = require('cookie-cutter');

var loginCheck = require('../modules/login-check')();
var helpers = require('../modules/helpers')();
var AutoSug = require('../modules/auto-sug.js');
var config = require('../../../config');
var userTemplate = require('../templates/user.hbs');


var user = JSON.parse(cookie.get('user'));
var userModel = Backbone.Model.extend({
  defaults: {
    'firstName': '',
    'lastName': '',
    'address': '',
  },
  initialize: function() {
    this.setIsStudent();
    this.on('change:userType', this.setIsStudent.bind(this));
  },

  setIsStudent: function() {
    if (this.get('userType') == 'student') {
      this.set('isStudent', true);
    } else {
      this.set('isStudent', false);
    }
  },
});

var UserView = Backbone.View.extend({
  url: '/api/accounts/user/' + user.id,
  template: userTemplate,
  el: '.profile__body',
  initialize: function() {
    this.fetchUser();
  },

  fetchUser: function() {
    var req = $.get(this.url);
    var _this = this;

    req.done(function(data) {
      _this.model = new userModel(data);
      _this.render();
    });
  },

  render: function() {
    this.$el.html(this.template(this.model.toJSON()));

    this.renderAddSkill();
    this.attachAutoComplete();
    this.attachValidate();
    return this;
  },

  renderAddSkill: function() {
    if (!this.model.get('editMode') || !this.model.get('linkedInDetails'))
    return;

    addSkillView.render();
    this.$el.find('.add-skills').html(addSkillView.autoSug.el);
  },

  attachAutoComplete: function() {
    var input = this.$el.find('.address input')[0];
    var _this = this;

    if (!input)
    return;

    this.autocomplete = new google.maps.places.Autocomplete(input);

    this.autocomplete.addListener('place_changed', function() {
      var place = _this.autocomplete.getPlace();

      _this.model.set('address', place.formatted_address)
      _this.model.set('latitude', place.geometry.location.lat().toFixed(4));
      _this.model.set('longitude', place.geometry.location.lng().toFixed(4));
    });
  },

  events: {
    'click .switch-type': 'updateUserType',
    'click .btn--pri': 'updateUserInfo',
    'click .email .edit': 'editUser',
    'click .email .val': 'showProfile',
    'click .logout': 'logoutUser',
    'click .skills .remove': 'removeSkill',
  },

  showProfile: function() {
    this.model.set('editMode', false);
    this.render();
  },

  removeSkill: function(e) {
    var target = $(e.target);
    var skill = target.siblings('.name').text();
    var skillArr = this.model.get('linkedInDetails').skills;
    var newSkillArr = _.without(skillArr, _.findWhere(skillArr, {name: skill}));
    var _this = this;

    this.model.get('linkedInDetails').skills = newSkillArr;
    target.parent().remove();

    this.render();
  },

  logoutUser: function() {
    var url = '/api/accounts/logout';
    var req = $.get(url);

    req.done(function(data) {
      cookie.set('user', '', { expires: new Date(0) , path: '/'});
      window.location.href = '/';
    });
  },

  editUser: function() {
    this.model.set('editMode', true);

    this.render();
  },

  attachValidate: function() {
    var _this = this;

    this.formValidator = new FormValidator({
      field: _this.$el.find('.form'),
      optionals: ['autosuggest'],
    });
  },

  updateUserInfo: function() {
    var _this = this;
    var req;

    this.formValidator.run(function(data){
      if (!data.isValid)
      return;

      delete data.result.autosuggest;

      data.result.latitude = _this.model.get('latitude');
      data.result.longitude = _this.model.get('longitude');
      data.result.editMode = false;

      if (_this.model.get('linkedInDetails'))
      data.result.skills = JSON.stringify(_this.model.get('linkedInDetails').skills);

      req = $.ajax({
        type: 'PUT',
        url: _this.url,
        data: data.result,
      })

      req.done(function(response) {
        _this.model.set('firstName', response.user.firstName);
        _this.model.set('lastName', response.user.lastName);
        _this.model.set('editMode',false);

        if (_this.model.get('linkedInDetails')) {
          _this.model.get('linkedInDetails').headline = data.result.headline;
          _this.model.get('linkedInDetails').industry = data.result.industry;
        }
        _this.render();
      });
    });
  },

  updateUserType: function() {
    var url = this.url;
    var _this = this;

    var req = $.ajax({
      type: 'PUT',
      url: url,
      data: {userType: 'teacher'}
    });

    req.done(function() {
      cookie.set('user', '', { expires: new Date(0) , path: '/'});
      cookie.set('user', JSON.stringify({id:  user.id, type: 'teacher'}))
      _this.model.set('userType', "teacher");
      _this.render();
    });
  },

});

var AddSkillView = Backbone.View.extend({
  url: '/api/skill',
  initialize: function() {
    var req = $.get(this.url);
    var _this = this;

    req.done(function(data) {
      _this.skillData = data;
    });
  },

  render: function() {
    var list = _.filter(this.skillData, function(obj){
      return !_.findWhere(userView.model.get('linkedInDetails').skills, {name: obj.name})
    });

    this.autoSug =  new AutoSug({
      list: list,
      placeholder: 'add skill',
    });

    this.autoSug.on('itemSelected', this.addSkillTolist.bind(this));
  },

  addSkillTolist: function(item) {
    userView.model.get('linkedInDetails').skills.push({name: item.name});
    userView.render();
  }
});

var userView =  new UserView();
var addSkillView =  new AddSkillView();