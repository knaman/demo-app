var $ = require('jquery');
var FormValidator = require('form-validatorjs');
var displayMsg = require('../modules/display-msg');

var tokenG;

var verifyToken = function verifyToken(token) {
  var url = '/api/accounts/reset/' + token;
  var req = $.get(url);
  var $info = $('.info');
  var $form = $('.form');
  var errMsg = 'Invalid token';

  req.done(function() {
    $form.removeClass('is-hidden')
  });

  req.fail(function() {
    $info.addClass('info--danger is-active')
      .html(errMsg);
  });
};

var getToken = function getToken () {
  var url = window.location.href;
  var startIndex = url.indexOf('reset') + 6;

  var token  = url.substring(startIndex, url.length);

  // global
  tokenG = token;
  verifyToken(token);
};

var processReq = function processReq(data, formValidator) {
  data.token = tokenG;

  var url = '/api/accounts/reset-done';
  var req = $.post(url, data);

  req.done(function(data) {
    formValidator.empty();
    displayMsg(data, 'success');
  });

  req.fail(function() {
    displayMsg(data, 'danger');
  });
};

var validateForm = function validateForm() {
  var $form = $('.form');
  var formValidator =  new FormValidator({
    field: $form,
    isSame: [{'password': 'password_again', 'message': 'Passwords not same.'}]
  })

  $form.find('.btn').on('click', function() {
    formValidator.run(function(data) {
      if (data.isValid)
      processReq(data.result, formValidator);
    });
  });
};

validateForm();
getToken();
