var $ = require('jquery');
var Backbone = require('backbone');
var _ = require('underscore');
var cookie = require('cookie-cutter');

var loginCheck = require('../modules/login-check')();
var helpers = require('../modules/helpers')();
var config = require('../../../config');
var displayMsg = require('../modules/display-msg');


var user = JSON.parse(cookie.get('user'));

var createGeoJson = function createGeoJson(lat, lng, title, color) {
  return  {
    "type": "Feature",
    "geometry": {
      "type": "Point",
      "coordinates": [lng ,lat]
    },
    "properties": {
      "title": title,
      'marker-size': 'small',
      'marker-color': color,
      'marker-symbol': 'circle',
    }
  };
};

var getOthersJson = function getOthersJson(others) {
  var arr = [];

  for (var i = 0 ; i < others.length; i++) {
    arr.push(createGeoJson(others[i].latitude, others[i].longitude, others[i].firstName + ' ' + others[i].lastName,'#1877D2'));
  }

  console.log(arr);
  return arr;
};

var setOthersMarker = function setOthersMarker(others, map) {
  var jsonArr = getOthersJson(others);
  var othersMarkerLayer = L.mapbox.featureLayer().addTo(map);

  othersMarkerLayer.on('layeradd', function(e) {
    console.log(e);
  });

  othersMarkerLayer.setGeoJSON(jsonArr);
};

var createRadius = function createRadius(lat, lng, radius, map) {
  var radiusLayer = L.mapbox.featureLayer().addTo(map);
  var circle = L.circle([lat, lng], radius, {
    color: '#80B8F1',
    fillColor: '#80B8F1',
    fillOpacity: 0.2
  });

  circle.addTo(radiusLayer);
};

var setMyMarker = function setMyMarker(lat, lng, title, map) {
  var json = createGeoJson(lat, lng, title, '#D27618');
  var myMarkerLayer = L.mapbox.featureLayer().addTo(map);

  myMarkerLayer.on('layeradd', function(e) {
    console.log(e);
  });

  myMarkerLayer.setGeoJSON(json);
};

var showMap = function showMap(data) {
  L.mapbox.accessToken = config.mapboxToken;

  var lat = data.user.latitude;
  var lng = data.user.longitude;

  var map = L.mapbox.map('mapbox',  config.mapboxId).setView([lat, lng], 14);
  var title = data.user.firstName + ' ' + data.user.lastName;
  var radius = 1000;

  setMyMarker(lat, lng, title, map);
  createRadius(lat, lng, radius, map);

  setOthersMarker(data.others, map);
}

var showMapFail = function showMapFail() {
  var lat = '12.9699';
  var lng = '77.6499';

  L.mapbox.accessToken = config.mapboxToken;
  var map = L.mapbox.map('mapbox',  config.mapboxId).setView([lat, lng], 14);
};

var addUserType = function addUserType() {
  var $footer = $('.footer');

  $footer.find('.user-type').text(user.type);

  $footer.find('.logout').on('click', function() {
    var url = '/api/accounts/logout';
    var req = $.get(url);

    req.done(function(data) {
      cookie.set('user', '', { expires: new Date(0) , path: '/'});
      window.location.href = '/';
    });
  });
};

var loadMap = function loadMap() {
  var url = '/api/';
  var getType = (user.type === 'student')?'teacher': 'student';
  var req = $.get(url + getType);

  req.done(function(data) {
    showMap(data);
    addUserType();
  });

  req.fail(function(data) {
    displayMsg(data, 'error');
    showMapFail();
    addUserType();
  });
};

loadMap();
