var $ = require('jquery');

var verifyToken = function verifyToken(token) {
  var url = '/api/accounts/signup/' + token;
  var req = $.get(url);
  var $info = $('.info');
  var succMsg = 'Successfully signed up. </br><a  class="link" href="/">Login</a>'
  var errMsg = 'Invalid token';

  req.done(function() {
    $info.addClass('info--success is-active')
      .html(succMsg);
  });

  req.fail(function() {
    $info.addClass('info--danger is-active')
      .html(errMsg);
  });
};

var getToken = function getToken () {
  var url = window.location.href;
  var startIndex = url.indexOf('signup') + 7;

  var token  = url.substring(startIndex, url.length);

  console.log(token);
  verifyToken(token);
};

getToken();
