var $ = require('jquery');
var FormValidator = require('form-validatorjs');
var cookie = require('cookie-cutter');

var SocialAuth = require('../modules/social-auth');
var config = require('../../../config');
var displayMsg = require('../modules/display-msg');

var formValidationOptions = {
  validateAgainst : {
    'email': 'emailReg'
  },
  regObj: {
    'emailReg': /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
  },
  invalidMsg: {
    'email': 'Invalid Email'
  }
};

var signupValidator = new FormValidator(
  $.extend(true, {field: $('.form--signup')}, formValidationOptions)
);

var loginValidator = new FormValidator(
  $.extend(true, {field: $('.form--login')},formValidationOptions)
);

var resetValidator = new FormValidator(
  $.extend(true, {field: $('.form--reset')},formValidationOptions)
);

var processReq = function processReq(data, type) {
  var reqUrl = {
    'signup': '/api/accounts/user',
    'login': '/api/accounts/login',
    'reset': '/api/accounts/reset',
    'loginFacebook': '/api/accounts/login/facebook',
  };

  var url = reqUrl[type];

  $.post(url, data)
    .done(function(response) {
      if (type === 'signup')
      signupValidator.empty();
      else if (type === 'login' || type === 'loginFacebook' ) {
        loginValidator.empty();
        cookie.set('user', JSON.stringify({id: response.user.id, type: response.user.userType}));

        setTimeout(function() {
          window.location.href = '/profile'  ;
        }, 1000);
      }

      else if (type === 'reset')
      resetValidator.empty();

      displayMsg(response, 'success');
    })
    .fail(function(response) {
      displayMsg(response, 'error');
    });
};

new SocialAuth({
  fb: {
    clientId: config.fb_id,
    button:  $('.btn--fb'),
    scope: 'public_profile, email, user_location',
    done: function(err, data) {
      if (!err) {
        processReq({access_token: data.token}, 'loginFacebook');
      } else {
        console.log(err);
      }
    },
  },
});


$('.form--signup .btn--pri').on('click', function() {
  signupValidator.run(function(data) {
    if (data.isValid) {
        data.userType = $('.signup-type').find('input:checked').val();
        processReq(data.result, 'signup');
    }
  });
});

$('.form--login .btn--pri').on('click', function() {
  loginValidator.run(function(data) {
    if (data.isValid)
    processReq(data.result, 'login');
  });
});

$('.form--reset .btn--pri').on('click', function() {
  resetValidator.run(function(data) {
    if (data.isValid)
    processReq(data.result, 'reset');
  });
});

var toggleForm = function toggleForm(node) {
  var klass = $(node).data('show');
  $('.form').addClass('is-hidden');
  $('.form--' + klass ).removeClass('is-hidden');
};

$('.form-title').on('click', function() {
  $('.form-title').removeClass('is-active');
  $(this).addClass('is-active');

  toggleForm(this);
});

$('.form--login .reset').on('click', function() {
  toggleForm(this);
});


