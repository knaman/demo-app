var Backbone = require('backbone');
var _ = require('underscore');

// backbone events have `on` and `off` already
// hence using _on and _off
function keyNavigation(options) {
  var obj  = {
    _on: function() {
      this._off();
      this.trackKeys();
    },

    _off: function() {
      this.$el.off('keydown');
    },

    trackKeys: function() {
      this.$el.on('keydown', this.whatKey.bind(this));
    },

    whatKey: function(e) {
      switch (e.keyCode) {
        case 40:
          this.handleKeyPress('down');
          break;
        case 38:
          this.handleKeyPress('up');
          break;
        case 13:
          this.handleKeyPress('enter');
          break;
      }
    },

    handleKeyPress: function(key) {
      if (key === 'up') this.goUp();
      if (key === 'down') this.goDown();
      if (key === 'enter') this.isEnter();
    },

    goUp: function() {
      if (this.currPosition < 1)
      return;

      this.currEl.set('isScrolled', false);

      this.currPosition--;
      this.currEl = this.collection.at(this.currPosition);
      this.currEl.set('isScrolled', true);

      this.ensureWithinView();
    },

    goDown: function() {
      if (this.currPosition >= this.listLength - 1)
      return;

      if (!this.collection.length)
      return;

      if (this.currPosition >= 0)
      this.currEl.set('isScrolled', false);

      this.currPosition++;
      this.currEl = this.collection.at(this.currPosition);
      this.currEl.set('isScrolled', true);

      this.ensureWithinView();
    },

    isEnter: function() {
      if (this.currEl)
      this.trigger('itemEntered', this.currEl.toJSON());
    },

    init: function() {
      this.currPosition = -1;
      this.topScrewed = -1;
      this.listLength = this.collection.length;
      this.currEl = null;
      this.maxEl = Math.floor(this.scrollOn / this.itemH);
      this.bottomScrewed =  this.maxEl;
    },

    reset: function() {
      if (!this.collection.length)
      return;

      this.currPosition = -1;

      this.topScrewed = -1;
      this.listLength = this.collection.length;

      // remove class from previous list scrolling
      if (this.currEl)
      this.currEl.set('isScrolled', false);

      this.currEl = null;
      this.$scrollEl.scrollTop(0);
      this.bottomScrewed =  this.maxEl;
    },

    ensureWithinView: function() {
      var itemH = this.itemH;
      var scrollTop = this.$scrollEl.scrollTop();
      var scrollReq;

      if (this.currPosition === this.bottomScrewed) {
        scrollReq = scrollTop +  itemH * 1;
        this.bottomScrewed++;
        this.topScrewed++;
      } else if (this.currPosition === this.topScrewed) {
        scrollReq = scrollTop - itemH * 1;
        this.bottomScrewed--;
        this.topScrewed--;
      }

      this.$scrollEl.scrollTop(scrollReq);
    },

  };

  _.extend(obj, options, Backbone.Events);
  obj.init();

  return obj;
}

module.exports = keyNavigation;
