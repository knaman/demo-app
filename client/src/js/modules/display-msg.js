var $ = require('jquery');

var displayMsg = function displayError(response, type) {
  var msg = [];
  var $info = $('.info');
  var content;
  var error;
  var klass;

  if (type === 'error') {
    klass = 'info--danger';
    error  = JSON.parse(response.responseText).error;
    if (typeof error === 'string') {
      content = error;
    } else {
      $.each(error, function(key, value) {
        $.each(value, function(i, err) {
          msg.push(err.message);
        });
      });

      content = msg.join(' </br> ');
    }
  } else {
    klass = 'info--success';
    content = response.message;
  }

  $info.html(content)
    .attr('class','info ' +  klass + ' is-active');
};

module.exports = displayMsg;