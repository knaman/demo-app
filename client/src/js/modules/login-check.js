var $ = require('jquery');

var loginCheck = function loginCheck() {
  var url = '/api/accounts/is_logged_in';
  var req = $.get(url);

  var isLandingPage = function isLandingPage() {
    var url = window.location.href;
    var urlSpl = url.split('/');
    var len = urlSpl.length;

    if (len <= 3)
    return true;
    else if (len === 4) {
      if (!urlSpl[len-1])
      return true;
    }

    return false;
  };

  req.done(function() {
    if(isLandingPage()){
      window.location.href = '/profile';
    }
  });

  req.fail(function() {
    if(!isLandingPage()){
      window.location.href = '/';
    }
  });
};

module.exports = loginCheck;
