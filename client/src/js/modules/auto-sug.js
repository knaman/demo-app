var Backbone = require('backbone');
var _ = require('underscore');

var keyNavigation = require('./key-navigation');
var autoSuggestSearchTpl = require('../templates/auto-sug-search.hbs');
var autoSuggestListItemTpl = require('../templates/auto-sug-list-item.hbs');

/**
 * autosuggest search  model
 */
var AutoSuggestSearchModel = Backbone.Model.extend({
  defaults: {
    name: '',
    isFocus: false,
    setInternally: false,
    placeholder: '',
  },
});

/**
 * autosuggest item  model
 */
var AutoSuggestItemModel = Backbone.Model.extend({
  defaults: {
    isScrolled: false,
  },
});

/**
 * autosuggest collection
 */
var AutoSuggestListColl = Backbone.Collection.extend({
  model: AutoSuggestItemModel,
});

/**
 * autosuggest each list item
 */
var AutoSuggestListItem = Backbone.View.extend({
  tagName: 'li',
  tpl: autoSuggestListItemTpl,
  className: ['auto-suggest__list-item'],
  initialize: function(options) {
    this.whatSelected = options.whatSelected;
    this.model.on('change:isScrolled', this.render.bind(this));
  },

  render: function() {
    this.$el.html(this.tpl(this.model.toJSON()));

    return this;
  },

  events: {
    'click .name': 'selectItem',
  },

  selectItem: function(e) {
    // stopped from not reaching the document click handler
    e.stopPropagation();

    this.model.set('isScrolled', true);
    this.whatSelected.trigger('itemSelected', this.model.toJSON());
  },
});

/**
 * auto suggestion list
 * default hidden
 */
var AutoSuggestList = Backbone.View.extend({
  tagName: 'ul',
  className:['auto-suggest__list is-hidden'],
  initialize: function(list, whatSelected) {
    this.collection = new AutoSuggestListColl(list);
    this.whatSelected  = whatSelected;
    this.listenTo(this.collection, 'reset', this.render);
  },

  render: function() {
    this.$el.html('');
    this.collection.each(this.renderItem.bind(this));

    return this;
  },

  renderItem: function(model) {
    var _this = this;

    var view = new AutoSuggestListItem({
      model: model,
      whatSelected: _this.whatSelected,
    });

    this.$el.append(view.render().el);
  },
});

/**
 * autosuggest search box
 * model: AutoSuggestSearchModel
 */
var AutoSuggestSearch = Backbone.View.extend({
  className: ['auto-suggest__search'],
  tpl: autoSuggestSearchTpl,
  initialize: function(selected, placeholder) {
    this.model = new AutoSuggestSearchModel(selected);
    this.model.set('placeholder', placeholder);
    this.model.on('change:isFocus', this.render.bind(this));
  },

  render: function() {
    var tpl = this.tpl;
    var model = this.model.toJSON();

    // used to prevent re-rendering if the model isFocus
    // set by the object itself
    if (model.setInternally) {
      this.model.set('setInternally', false);
      return;
    }

    this.$el.html(tpl(model));

    // if model = { name: 'a, b'} || {name: 'a b'}
    // handlebars is rendering only `a` in the input box
    // temp fix
    this.$el.find('input').val(model.name);

    if (model.isFocus) {
      this.$el.find('input').focus();
    }

    this.$el.attr('field-name', 'autosuggest');
    return this;
  },

  events: {
    'click input': 'setFocus',
    'keyup input': 'setText',
  },

  setFocus: function(e) {
    e.stopPropagation();
    this.model.set('setInternally', true);
    this.model.set('isFocus', true);
  },

  setText: function(e) {
    this.model.set('name', e.target.value);
  },
});

/**
 * main container for autosuggest
 * contains search box and dropdown
 */
var AutoSug = Backbone.View.extend({
  className: ['auto-suggest'],
  initialize: function(options) {
    var _this = this;

    // object keep track of selected item in the list
    // is passed upto the list item and triggered from there
    this.whatSelected = _.extend({}, Backbone.Events);

    this.options = options;
    this.autoSuggestSearch = new AutoSuggestSearch(options.selected, options.placeholder);
    this.autoSuggestList = new AutoSuggestList(options.list, this.whatSelected);

    // keynavigation mixin
    this.keyNavigation =  keyNavigation({
      $el:  _this.$el,
      $scrollEl: _this.autoSuggestList.$el,
      collection: _this.autoSuggestList.collection,
      scrollOn: 100,
      itemH: 25,
    });

    this.autoSuggestSearch.model.on('change:isFocus', this.showList, this);
    this.autoSuggestSearch.model.on('change:name', this.filterList, this);
    this.whatSelected.on('itemSelected', this.itemSelected.bind(this));
    this.keyNavigation.on('itemEntered', this.itemSelected.bind(this));

    this.render();
  },

  render: function() {
    this.$el.html('');

    this.$el.append(this.autoSuggestSearch.render().el);
    this.$el.append(this.autoSuggestList.render().el);

    return this;
  },

  showList: function() {
    if (this.autoSuggestSearch.model.get('isFocus')) {
      this.autoSuggestList.$el.removeClass('is-hidden');
      this.trigger('showList');
      this.keyNavigation._on();
    } else {
      this.keyNavigation._off();
    }
  },

  hideList: function() {
    this.autoSuggestList.$el.addClass('is-hidden');
    this.autoSuggestSearch.model.set('isFocus', false);
  },

  filterList: function() {
    var list = this.options.list;
    var filterBy = this.autoSuggestSearch.model.get('name').toLowerCase();
    var filterByLen = filterBy.length;

    var newList =  _.filter(list, function(item) {
      if (item.name.toLowerCase().substring(0, filterByLen) === filterBy)

      return true;
    });

    this.autoSuggestList.collection.reset(newList);
    this.keyNavigation.reset();
  },

  itemSelected: function(item) {
    this.options.selected = item;

    this.autoSuggestSearch.model.set('name', item.name, {silent: true});
    this.autoSuggestSearch.model.set('isFocus', false);
    this.autoSuggestSearch.render();
    this.hideList();
    this.trigger('itemSelected', item);
  },
});


/**
 * creates html for the autosuggest with
 * an input box and dropdown for suggestions
 * @type {function}
 */
module.exports = AutoSug;
