(function(root, factory) {
  'use strict';

  if (typeof exports === 'object') {

    // CommonJS module
    // Load jQuery as a dependency
    var jQuery;
    try {jQuery = require('jquery'); } catch (e) {}

    module.exports = factory(jQuery);
  } else {
    root.SocialAuth = factory(root.jQuery);
  }
}

(this, function($) {
  'use strict';

  var getUserInfo =  function getUserInfo(token, options) {
    FB.api('/me',
      function(res) {
        res.token = token;
        options.done(null, res);
      }
    );
  };

  var checkLoginStateFb =  function checkLoginState(response, options) {
    if (response.status === 'connected') {
      getUserInfo(response.authResponse.accessToken, options);
    } else if (response.status === 'not_authorized') {
      options.done('the person is logged into Facebook, but not your app');
    } else {
      options.done('the person is not logged into Facebook, so were not sure if they are logged into this app or not.');
    }
  };

  var attachFbButton = function attachFbButton(options) {
    options.button.click(
      function(e) {
        e.preventDefault();
        FB.login(
          function(response) {
            checkLoginStateFb(response, options);
          },
          {scope: options.scope}
        );
      }
    );
  };

  var attachGoogleButton = function attachGoogleButton(auth2, options) {
    options.button.on('click', function(e) {
      e.preventDefault();
    });

    auth2.attachClickHandler(options.button[0], {},
      function(googleUser) {
        options.done(null, googleUser);
      }, function(error) {
        options.done(error);
      }
    );
  };

  var loadFbSdk = function loadFbSdk() {
    // Load the SDK asynchronously
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  };

  var initGoogle = function initGoogle(options) {
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      var auth2 = gapi.auth2.init({
        client_id: options.clientId,
        scope: options.scope,
      });

      attachGoogleButton(auth2, options);
    });
  };

  var initFb = function initFb(options) {
    loadFbSdk();
    attachFbButton(options);

    window.fbAsyncInit = function() {
      FB.init({
        appId      : options.clientId,
        cookie     : true,  // enable cookies to allow the server to access
                            // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.2' // use version 2.2
      });
    };
  };

  var SocialAuth = function SocialAuth(options) {
    if (typeof options.fb !== 'undefined')
    initFb(options.fb);

    if (typeof options.google !== 'undefined')
    initGoogle(options.google);
  };

  return SocialAuth;
}));

